class Hamburger {
    constructor(size, stuffing){
        this._size = size;
        this._stuffing = stuffing;
        this._toppings = [];

        try{
            if(arguments.length !== 2){
                throw new Error( "Вы ввели один аргумент! Введите два аргумента!");
            }
            if(size !== Hamburger.size_small && size !== Hamburger.size_large){

                throw new Error("Вы передали не правильные названия размеров!");
            }
            if(stuffing !== Hamburger.stuffing_salad && stuffing !==  Hamburger.stuffing_cheese && stuffing !== Hamburger.stuffing_potato){
                throw new Error("Вы добавили много добавок");
            }

        }catch (e) {
            console.log(e)
        }
    }


    static size_small = {calories: 50, price: 20};
    static size_large = {calories: 40, price: 100};
    static stuffing_cheese = {calories: 20, price: 10};
    static stuffing_salad = {calories: 5, price: 20};
    static stuffing_potato = {calories: 15, price: 10};
    static topping_mayo = {calories: 5, price: 20};
    static topping_spice = {calories: 0, price: 10};

    addTopping = function(topping){
        this._toppings.push(topping);
    };

    getSize = function (){
        return this._size;
    };
    getStuffing = function (){
        return this._stuffing;
    };
    calculatePrice = function (){
        let price = 0;
        this._toppings.forEach(elem => price+=elem.price);
        price += this._size.price + this._stuffing.price;
        return price;

    };
    calculateCalories = function (){
        let calories = 0;
        this._toppings.forEach(elem => calories+=elem.calories);
        calories += this._size.calories + this._stuffing.calories;
        return calories;
    };

}


function HamburgerException(message){
    this.name = "Hamburger Exception";
    this.message = message;
}


const cl = new Hamburger(Hamburger.size_small, Hamburger.stuffing_cheese);
console.log(cl.calculateCalories());
console.log(cl.calculatePrice());

cl.addTopping(Hamburger.topping_mayo);

console.log(cl);




