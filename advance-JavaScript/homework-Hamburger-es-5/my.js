function Hamburger(size, stuffing) {

    try{
        if(arguments.length !== 2){
            throw new Error( "Вы ввели один аргумент! Введите два аргумента!");
        }
        if(size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_LARGE){

            throw new Error("Вы передали не правильные названия размеров!");
        }
        if(stuffing !== Hamburger.STUFFING_SALAD && stuffing !==  Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO){
            throw new Error("Вы добавили много добавок");
        }

    }catch (e) {
        console.log(e)
    }

    this._size = size;
    this._stuffing = stuffing;
    this._toppings = [];
}

Hamburger.SIZE_SMALL = {calories: 50, price: 20};
Hamburger.SIZE_LARGE = {calories: 40, price: 100};
Hamburger.STUFFING_CHEESE = {calories: 20, price: 10};
Hamburger.STUFFING_SALAD = {calories: 5, price: 20};
Hamburger.STUFFING_POTATO = {calories: 15, price: 10};
Hamburger.TOPPING_MAYO = {calories: 5, price: 20};
Hamburger.TOPPING_SPICE = {calories: 0, price: 10};

Hamburger.prototype.addTopping = function (topping){
    this._toppings.push(topping);
};
Hamburger.prototype.getSize = function (){
    return this._size;
};
Hamburger.prototype.getStuffing = function (){
    return this._stuffing;
};

Hamburger.prototype.calculatePrice = function (){
    let price = 0;
    this._toppings.forEach(elem => price+=elem.price);
    price += this._size.price + this._stuffing.price;
    return price;

};
Hamburger.prototype.calculateCalories = function (){
    let calories = 0;
    this._toppings.forEach(elem => calories+=elem.calories);
    calories += this._size.calories + this._stuffing.calories;
    return calories;
};

function HamburgerException(message){
    this.name = "Hamburger Exception";
    this.message = message;
}



const humb = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
console.log(humb.calculatePrice());
console.log(humb.calculateCalories());
humb.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);

console.log(humb);
















