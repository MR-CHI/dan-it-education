class Game {
    constructor() {
        this.human = document.querySelector('#human');
        this.pc = document.querySelector('#pc');
        this.btnLevel = document.querySelectorAll('.button');
        this.outArray = [];
        this.getRandomObject(this.creatTable(10,10));
        this.table = document.querySelector('table');
        this.humanClick()
    }
    // getSpeed = (level) => {
    //     switch (level) {
    //         case 'Light':
    //             return 1500;
    //
    //         case 'Medium':
    //             return 1000;
    //
    //         case 'Hard':
    //             return 500;
    //
    //         default:
    //             return 'Light';
    //     }
    // };


    creatTable(td, tr){
        const table = document.createElement('table');
        table.classList.add('table-striper');
        const tableColumns = Array(td).fill("<td></td>").join("");
        const tableRow = Array(tr).fill(`<tr>${tableColumns}</tr>`).join("");
        table.insertAdjacentHTML("beforeend", tableRow);
        document.body.append(table)
        const tableRef = document.querySelector('.table-striper')
        const cellsRef = [...document.querySelectorAll('td')]

        return {
            tableRef,
            cellsRef
        }
    }
    getRandomObject(arr){
        const { cellsRef } = arr
        const interval = setInterval(() => {
            if ( cellsRef.length ) {
                const searchRandomArr = Math.floor(Math.random() * cellsRef.length);
                const randomObj = cellsRef.splice( searchRandomArr, 1 );
                const obj = randomObj.find((item => item))
                obj.classList.add( 'active' )
                this.outArray.push(obj)
                const int = setInterval(() =>{
                    if(obj.classList.contains('active')){
                        obj.classList.remove('active');
                        obj.classList.add('red')
                        this.pc.innerHTML++
                        if (this.pc.innerHTML == 50){
                            clearInterval(interval)

                            console.log('WINER' + ' ' + 'PC')
                        }
                    }
                    else if (obj.length === 0){
                        clearInterval(interval)
                    }
                    },1500)
            }

            else {
                clearInterval( interval )

            }
        }, 1500)


    }
    humanClick(){
        this.table.addEventListener('click', (event)=> {
            const td = event.target
            if (td.classList.contains('active'))  {
                td.classList.remove('active')
                td.classList.add('green')
                this.human.innerHTML++
            }
        })

    }


}
const game = new Game();






