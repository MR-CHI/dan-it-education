import React from 'react';
import './App.css';

import BookApp from "./component/bookApp/BookApp";

function App() {
    return(
        <div>
            <BookApp />
        </div>
    )

}

export default App;
