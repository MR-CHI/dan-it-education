import React from "react";

const Input = (props) => {
    const {type = "text", value, name, placeholder, className, handelChange} = props;
    return <input type={type} value={value}  name={name} placeholder={placeholder} className={className} onChange={handelChange}/>

}

export default Input