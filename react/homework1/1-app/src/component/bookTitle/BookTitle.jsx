import React from "react";

const BookTitle = (props) =>{
    return(
        <div>
            <div className="container d-flex justify-content-center">
                <div className="text-secondary"><i className="fas fa-book-open text-primary"></i> {props.title} <span>{props.list}</span></div>
            </div>
        </div>
    )
}

export default BookTitle