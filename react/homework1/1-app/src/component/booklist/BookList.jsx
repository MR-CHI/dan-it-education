import React from "react";
import BookListItem from "../TableBook/BookListItem";

const BookList = ({list, onDeleteBook, onEditBook}) => {

    const bookElements = list.map((item) => <BookListItem onDeleteBook={onDeleteBook} onEditBook={onEditBook} {...item}  />);
    return (
        <table class="table table-striped mt-2 container">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>ISBN#</th>

            </tr>
            </thead>
            <tbody  id="book-list">{bookElements}</tbody>
        </table>
    );
};

export default BookList;