import React, {Component} from "react";
import BookTitle from "../bookTitle/BookTitle";
import Form from "../form/Form";
import BookListItem from "../TableBook/BookListItem";
import BookList from "../booklist/BookList";
import Count from "../count/Count";


class BookApp extends Component{
    state = {
        list: []
    }
    onAddBook = (title, author, isbn) =>{
        const newList = {
            title: title,
            author: author,
            isbn: isbn,
        }
        this.setState(({list}) => {
            return{
                list: [...list, newList]
            }
        })
    }
    onDeleteBook = () =>{
        const {list} = this.state
        this.setState(({ list }) => {
            const idx = list.findIndex((el) => el.title !== list.title);
            const newArray = [...list.slice(0, idx), ...list.slice(idx + 1)];
            return {
                list: newArray
            };
        });
    }
    onEditBook = () =>{



    }


    render() {

        const { list } = this.state;
        const { onAddBook, onDeleteBook, onEditBook} = this;
        const BookListProps = {
            onDeleteBook,
            onEditBook,
            list
        };
        return(
            <div>
                <BookTitle title='Book' list='list' />
                <Form onAddBook={onAddBook}/>
                <Count count={list.length}/>
                <BookList {...BookListProps} />

            </div>

        )
    }
}

export default BookApp