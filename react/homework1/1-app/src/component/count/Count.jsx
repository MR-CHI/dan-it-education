import React from "react";

const Count = (props) =>{
    return (
        <div className="container">
            <h3 id="book-count" className="book-count mt-5">Всего книг: {props.count}</h3>
        </div>
    )
}

export  default Count