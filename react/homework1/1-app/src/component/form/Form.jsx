import React, {Component} from "react";
import Input from "../input/Input";


class Form extends Component{
    state = {
        title: '',
        author: '',
        isbn: '',

    }
    handelChange = ({target}) =>{
        this.setState({
            [target.name]: target.value
        })
    }
    handelSubmit = (event) =>{
        event.preventDefault()
        this.setState({
            title: this.state.title,
            author: this.state.author,
            isbn: this.state.isbn,

        })
        const {title, author, isbn} = this.state
        this.props.onAddBook(title, author, isbn)

    }
    render() {
        const {title, author, isbn} = this.state;
        return(
            <div className="container">
                <form onSubmit={this.handelSubmit}>
                    <label className="form">Title
                        <Input name="title" type="text" value={title} handelChange={this.handelChange} />
                    </label>
                    <label className="form">author
                        <Input name="author" type='text' value={author} handelChange={this.handelChange} />
                    </label>
                    <label className="form">isbn
                        <Input name="isbn" type='text' value={isbn} handelChange={this.handelChange} />
                    </label>
                    <button >Add Book</button>
                </form>
            </div>
        )
    }
}
export default Form