import React from "react";

const BookListItem = ( {title, author, isbn, onDeleteBook, onEditBook}) => {

    return (
        <tr data-id="12">
            <td>{title}</td>
            <td>{author}</td>
            <td>{isbn}</td>
            <td >
                <a onClick={onEditBook} href="#" class="btn btn-info btn-sm"><i  class="fas fa-edit"></i></a>
            </td>
            <td>
                <a onClick={onDeleteBook} href="#" class="btn btn-danger btn-sm btn-delete">X</a>
            </td>
        </tr>
    );
};

export default BookListItem;