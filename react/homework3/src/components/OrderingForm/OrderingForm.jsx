import React from 'react';
import { useFormik } from 'formik';
import Input from "../../shared/Input/Input";
import {fields} from "./fields";
import './style.scss'
import * as Yup from 'yup';



// const validate = values => {
//     const errors = {};
//     if (!values.name) {
//         errors.name = 'Это поле обязательно к заполнению';
//     } else if (values.name.length > 10) {
//         errors.name = 'Должно быть не более 10 символов.';
//     }
//
//     if (!values.lastName) {
//         errors.lastName = 'Это поле обязательно к заполнению';
//     } else if (values.lastName.length > 10) {
//         errors.lastName = 'Должно быть не более 10 символов';
//     }
//     // if (!values.email) {
//     //     errors.email = 'Это поле обязательно к заполнению';
//     // } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
//     //     errors.email = 'не верный електронный адрес';
//     // }
//     if(!values.fullName){
//         errors.fullName = 'Это поле обязательно к заполнению';
//     }else if (values.fullName.length > 10){
//         errors.fullName = 'Должно быть не более 10 символов'
//     }
//
//     return errors;
// };


const OrderingForm = () => {

    const formik = useFormik({
        initialValues: {
            name: '',
            lastName: '',
            fullName : '',
            email: '',
            number: ''

        },
        validationSchema: Yup.object({
            name: Yup.string()
                .max(10, 'Должно быть не более 10 символов')
                .required('Это поле обязательно к заполнению'),
            lastName: Yup.string()
                .max(20, 'Должно быть не более 10 символов')
                .required('Это поле обязательно к заполнению'),
            fullName : Yup.string()
                .max(10, 'Должно быть не более 10 символов')
                .required('Это поле обязательно к заполнению'),
            email: Yup.string().email('Invalid email address').required('Это поле обязательно к заполнению'),
        }),
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (

        <form className='containerForm' onSubmit={formik.handleSubmit} >

            <Input {...formik.getFieldProps('name')} {...fields.name} />
                {formik.touched.name && formik.errors.name ? (<div className='error'>{formik.errors.name}</div>) : null}

            <Input {...fields.lastName} {...formik.getFieldProps('lastName')} />
                {formik.touched.lastName && formik.errors.lastName ? (<div className='error' >{formik.errors.lastName}</div>) : null}

            <Input {...fields.fullName} {...formik.getFieldProps('fullName')} />
                {formik.touched.fullName && formik.errors.fullName ? (<div className='error' >{formik.errors.fullName}</div>) : null}

            <Input {...fields.email} {...formik.getFieldProps('email')}/>
                {formik.touched.email && formik.errors.email ? (<div className='error' >{formik.errors.email}</div>) : null}

            {/*<Input {...fields.number} />*/}
            <button type="submit">Submit</button>

        </form>
    );
};

export default OrderingForm


