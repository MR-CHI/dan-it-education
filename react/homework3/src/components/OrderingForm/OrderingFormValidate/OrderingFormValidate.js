
export const OrderingFormValidate = values => {
    const errors = {};
    if (!values.name) {
        errors.name = 'Это поле обязательно к заполнению';
    } else if (values.name.length > 10) {
        errors.name = 'Должно быть не более 10 символов.';
    }

    if (!values.lastName) {
        errors.lastName = 'Это поле обязательно к заполнению';
    } else if (values.lastName.length > 10) {
        errors.lastName = 'Должно быть не более 10 символов';
    }

    // if (!values.email) {
    //     errors.email = 'Это поле обязательно к заполнению';
    // } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    //     errors.email = 'не верный електронный адрес';
    // }
    if(!values.fullName){
        errors.fullName = 'Это поле обязательно к заполнению';
    }else if (values.fullName.length > 10){
        errors.fullName = 'Должно быть не более 10 символов'
    }

    return errors;
};