export const fields = {
    email: {
        label: "Email",
        type: "email",
        name: "email",
        placeholder: "email"
    },
    name : {
        label : 'Ваше имя',
        type: 'text',
        name: 'name',
        placeholder: 'Введите Ваше имя'
    },
    lastName : {
        label: 'Ваша фамилия',
        type: 'text',
        name: 'lastName',
        placeholder: 'Введите Вашу фамилию'
    },
    fullName : {
        label: 'Ваше отчество',
        type: 'text',
        name: 'fullName',
        placeholder: 'Ваше отчество'
    },
    number: {
        label: 'Номер телефона',
        type: 'number',
        name: 'number',
        placeholder: 'number'
    }



};