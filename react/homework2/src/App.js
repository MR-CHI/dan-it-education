import React, {useState} from 'react';
import './App.scss';
import {createStore} from "redux";
import CardsProducts from "./components/client/CardsProducts/CardsProducts";
import 'bootstrap/dist/css/bootstrap.min.css';
import {reducer} from "./components/store/reducer";
import {Provider} from "react-redux";
export const initialCards = [
    {
        nameProduct: 'Banana',
        price: 13,
        id: 1,
        counter: 1

    },
    {
        nameProduct: 'Lemon',
        price: 12,
        id: 2,
        counter: 1

    },
    {
        nameProduct: 'Orange',
        price: 10,
        id: 3,
        counter: 1


    }
]
const store = createStore(reducer)
function App() {
    return(
        <Provider store={store}>
            <div className='container' >
                <div>
                    <CardsProducts productList={initialCards} />
                </div>
            </div>

        </Provider>

    )
}
export default App;
