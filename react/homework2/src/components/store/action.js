const openModal = () =>{
    return{
        type: 'OPEN_MODAL_BASKET',
    }
}
const closeModal = () =>{
    return{
        type : 'CLOSE_MODAL_BASKET'
    }

}
const addProduct = (item) =>{
    return{
        type: 'ADD_PRODUCT',
        item: item,
    }
}
const removeProduct = (id) =>{
    return{
        type: 'REMOVE_PRODUCT',
        id: id
    }

}
const incrementProduct = (id) =>{
    return{
        type: 'INCREMENT',
        id

    }
}
const decrementProduct = (id) =>{
    return{
        type: 'DECREMENT',
        id
    }
}
const clearBasket = () =>{
    return{
        type: 'CLEAR',
    }
}



export default {openModal, closeModal, addProduct, removeProduct, incrementProduct, decrementProduct, clearBasket}