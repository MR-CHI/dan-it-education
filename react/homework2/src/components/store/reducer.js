import {unmountComponentAtNode} from "react-dom";

const initialState = {
    modalIsOpen : false,
    products : []
}
export const reducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'OPEN_MODAL_BASKET' :
            return {...state, modalIsOpen: true}
        case 'CLOSE_MODAL_BASKET' :
            return {...state, modalIsOpen: false}
        case 'ADD_PRODUCT' :
            const newProducts = [...state.products, {...action.item}]
            const result = state.products.findIndex(product => product.nameProduct === action.item.nameProduct) !== -1;
            if(result){
                const result = state.products.map((item) => item.counter = 0);
            }else {
                return {...state, products: newProducts}
            }
            case 'REMOVE_PRODUCT' :
            const id = action.id;
            return {
                ...state,
                products: state.products.filter((item) => item.id !== id)
            }
        case 'INCREMENT' :
            const basket = state.products.map((item) => ({...item}));
            const basketIdx = basket.findIndex(({id}) => id === action.id);
            basket[basketIdx].counter++
            return {...state, products: basket}
        case 'DECREMENT' :
            const newBasket = state.products.map((item) => ({...item}));
            const newBasketIdx = newBasket.findIndex(({id}) => id === action.id);
            newBasket[newBasketIdx].counter--
            if(newBasket[newBasketIdx].counter === 0){
                newBasket.splice(newBasketIdx, 1)
            }

            return {...state, products: newBasket}
        case 'CLEAR' :
            return {
                ...state, products: []
            }

            default : return state

    }

}

