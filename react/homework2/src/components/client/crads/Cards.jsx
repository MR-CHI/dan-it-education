import React from "react";
import addProduct from '../../store/action'
import {useDispatch} from "react-redux";
import Button from "../../shared/Button/Button";
const Cards = (props) =>{
    const dispatch = useDispatch()
    const {nameProduct, price, id ,className, onClick} = props
    return(
        <div id={id}>
            {/*<Button text='-' />*/}
            <p>Name Product: {nameProduct}</p>
            {/*<Button text='+' />*/}
            <p>Price: {price}</p>
            <Button onClick={()=>dispatch(addProduct.addProduct(props))} className='btn btn-info' text='ADD +' />
        </div>
    )
}

export default Cards