import React from "react";
import './style.scss'
import Button from "../../shared/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import removeProduct from '../../store/action'
import incrementProduct from '../../store/action'
import decrementProduct from '../../store/action'

const ProductItem = ({nameProduct, price, id, counter}) =>{
    const dispatch = useDispatch()
    return(
        <div className='productItem alert alert-success' >
            <div>Ед: {counter}</div>
            <Button  text='-' onClick={() => dispatch(decrementProduct.decrementProduct(id))}/>
            <div>Цена товара : {price}</div>
            <Button onClick={()=>dispatch(incrementProduct.incrementProduct(id))} text='+' />
            <div>Название товара : {nameProduct}</div>
            <Button onClick={ ()=> dispatch(removeProduct.removeProduct(id))} className='btn btn-danger' text='удалить'/>
        </div>
    )
}
export default ProductItem