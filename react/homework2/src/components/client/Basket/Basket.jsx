import React from "react";
import './basket.scss'
import Button from "../../shared/Button/Button";
import closeModal from '../../store/action'
import clearBasket from '../../store/action'
import {useDispatch, useSelector} from "react-redux";
import ProductItem from "../ProductItem/ProductItem";
const Basket = ({modalIsOpen}) =>{
    const state = useSelector(state=> state.modalIsOpen);
    const products = useSelector(state => state.products);
    const current = products.map(item => item.price * item.counter);
    const sum = current.reduce((a, b) => a + b, 0)
    const productsItems = products.map((item) => <ProductItem {...item}/>)
    const dispatch = useDispatch()
    const style = {
        display: state ? 'block' : 'none'
    }
    return(
        <div style={style} className='basketContainer'>
            <div className='products'>
                {productsItems}
            </div>
            <div className='footerBasket'>
                <div>Total price: {sum}</div>
                <div>
                    <Button text='Очистить корзину' onClick={() => dispatch(clearBasket.clearBasket())} />
                    <Button text='Закрыть' onClick={()=>dispatch(closeModal.closeModal(modalIsOpen))}/>
                </div>

            </div>
        </div>

)
}
export default Basket