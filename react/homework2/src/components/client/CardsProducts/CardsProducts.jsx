import React from "react";
import Cards from "../crads";
import Button from "../../shared/Button/Button";
import openModal from '../../store/action'
import Basket from "../Basket/Basket";
import {useDispatch, useSelector} from "react-redux";
const CardsProducts = ({productList, modalIsOpen}) =>{
    const dispatch = useDispatch()
    const current = useSelector(state => state.products)
    const showProduct = productList.map((item) => <Cards {...item}/>)
    return(
        <div>
            <div>
                <Button onClick={()=>dispatch(openModal.openModal(modalIsOpen))} className={'btn btn-info'} text={`BASKET : ${current.length} `} />
            </div>
            <Basket />
            <div className={'navBar'}>
                {showProduct}
            </div>

        </div>
    )
}
export default CardsProducts