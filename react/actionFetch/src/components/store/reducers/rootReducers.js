import {combineReducers} from "redux";
import {persons} from "./persons";


const rootReducers = combineReducers({
    persons
})
export default rootReducers


