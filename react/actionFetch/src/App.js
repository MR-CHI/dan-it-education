import React from 'react';
import './App.css';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import {composeWithDevTools} from "redux-devtools-extension";
import rootReducers from '../src/components/store/reducers/rootReducers'
// import {reducer} from "./components/store/reducer";
import ToDo from "./components/client/ToDo/ToDo";

const store = createStore(rootReducers, composeWithDevTools(applyMiddleware(thunk)))

function App() {
    return (
      <Provider store={store}>
          <div>
              <ToDo />
          </div>
      </Provider>


  );
}

export default App;
