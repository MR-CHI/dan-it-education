function createNewUser() {

    const firstName = prompt('firstName');
    const lastName = prompt('lastName');
    const birthday = prompt('date');

    const newUser = {};

    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthday;

    newUser.getLogin = function () {

        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };

    newUser.getAge = function () {
        let age = new Date();
        let result = age.getFullYear() - this.birthday.split('.')[2];
        if (age.getMonth() + 1 - this.birthday.split('.')[1] <= 0) {
            return result - 1;
        } else {
            return result;
        }

    };


    newUser.getPassword = function () {
        return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.split('.')[2];
    }

    return newUser;

}


const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
console.log(user);

//
// Ответ на теоретический вопрос!
//
//
//     Экранирование служит для того, что бы к примеру,  нам нужно что-то передать в кавычках
// и мол интерпретатор не будет рассматривать кавычки как закрывающиеся.

