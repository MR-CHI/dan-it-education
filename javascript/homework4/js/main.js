function createNewUser() {
    const newUser = {};
    const firstName = prompt('firstName');
    const lastName = prompt('lastName');

    newUser.firstName = firstName;
    newUser.lastName = lastName;

    newUser.getLogin = function () {

        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());

// Ответ на теоретический вопрос:
//
//     Функции, которые находятся в объекте в качестве его свойств, называются «методами».
//
// например: this

