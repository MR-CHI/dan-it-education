const imageItem = document.querySelectorAll('img');

function show() {
    for (let i = 0; i < imageItem.length; i++) {
        if (imageItem[i].classList.length === 2) {
            imageItem[i].classList.remove('active');

            if (i === imageItem.length-1) {
                imageItem[0].classList.add('active');
            } else {
                imageItem[i + 1].classList.add('active');
            }
            break
        }
    }
}

const button = document.createElement('button');
button.innerText = 'STOP';
document.body.appendChild(button);

let timerId = setInterval(show, 1000);



button.addEventListener('click', (element) => {
    clearInterval(timerId);
});

const buttonPlus = document.createElement('button');
buttonPlus.innerText = 'START';
document.body.appendChild(buttonPlus);

buttonPlus.addEventListener('click', (element) =>{
    timerId = setInterval(show, 1000);
});


