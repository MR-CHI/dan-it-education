const dataUser =document.querySelectorAll('.btn');

document.addEventListener('keydown', (e) => {
    dataUser.forEach(element => {
        if (element.innerText.toLowerCase() === e.key.toLowerCase()) {
            const activKey = document.querySelector('.item');
            if (activKey) {
                activKey.classList.remove('item');
            }
            element.classList.add('item');
        }
    })
});