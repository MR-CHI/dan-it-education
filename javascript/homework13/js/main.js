const itemInput = document.getElementById('changeTheme');
const itemTheme = document.querySelector('body');

if (!localStorage.getItem('currentTheme')) {
    localStorage.setItem('currentTheme', 'light');
}
if (localStorage.getItem('currentTheme') === 'dark'){
    changeTheme()
}
itemInput.addEventListener("click", (event) => {
    if(localStorage.getItem('currentTheme') === 'dark'){
        localStorage.setItem('currentTheme', 'light');
        changeTheme()
    }else{
        localStorage.setItem('currentTheme', 'dark')
        changeTheme()
    }
});

function changeTheme() {
    itemTheme.classList.toggle('dark');
}





