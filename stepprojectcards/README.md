Step Project 3 Cards


Условием корректной работы системы является запуск проекта через liveserver.
Login - fanni@fanni
Password - 1234567890

Students

Malevych Fanni 
Makhortykh Ruslan
Dug Roman


Malevych Fanni 
    
    - Создание класса Visit, описывающий визит;
    - написание дочерних классов VisitDentist, VisitCardiologist, VisitTherapist;
    - добавление классам метод render,  возвращающий DOM-элемент карточки;
    - Создание  функционала редактирования карточек с использованием классов CardioVisitForm, TherapistVisitForm и DentistVisitForm;
    - Drag & Drop перемещение карточек;
    - Создание функции вывода на страницe всех карточек, полученных в GET-запросе
    - создание функции удаления карточки и редактирования
    - фильтр карточек

Makhortykh Ruslan

    - Макет проекта
    - Создание универсальных классов Input, Select, Textarea
    - Создание класса Modal (создает каркас всплывающего окна)
    - Создание класса ModalLogin и ModalAddVisit на основе класса Modal
    - Создание функции авторизации пользователя

Dug Roman

    - Создание универсального класса Form
    - Создания класса LoginForm (выводит форму авторизации)
    - Создания класса SelectVisitForm (вывод списка врачей и добавление визита нужного врача)
    - Создание класса CardioVisitForm TherapistVisitForm DentistVisitForm (создают соответствующие формы)
    - Создание функции добавление карточки
    - Стилизация всего макета


