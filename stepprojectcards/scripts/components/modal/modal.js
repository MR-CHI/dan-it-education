import {Component} from "../component.js";
export class Modal extends Component{
    content = `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                 </button>`
    renderModal(){
        const modalContainer = this.createElement('div', this.props, this.content)
        const closeBtn = modalContainer.querySelector('.close')
        closeBtn.addEventListener('click', function (){
            this.parentElement.remove()
        })
        return modalContainer
    }
}
