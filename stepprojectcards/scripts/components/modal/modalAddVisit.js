import {SelectForm} from "../select/selectForm.js";
import {Modal} from "./modal.js";


export class ModalAddVisit extends Modal{
    modalProps ={
        className: 'modal add-visit-modal'
    }
    render(){
        const modal = new Modal(this.modalProps)
        const elem = modal.renderModal()
        const selectDoctor = new SelectForm()
        elem.append(selectDoctor.render())
        return elem
    }
}