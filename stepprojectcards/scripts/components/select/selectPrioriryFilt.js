import {Select} from "./select.js";

export class SelectPrioriryFilt extends Select {
    selectProps ={
        className: 'select-priority-filter'
    }
    render(){
        const elObj = new Select(this.selectProps)
        const el = elObj.render()
        const optionDefault = new Option('Select priority')
        const optionCommon = new Option('Обычная')
        const optionPriority = new Option('Приоритетная')
        const optionUrgent = new Option('Неотложная')
        el.append(optionDefault,optionCommon,optionPriority,optionUrgent)
        return el
    }
}