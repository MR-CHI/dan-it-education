import {Select} from "./select.js";

export class SelectDocFilt extends Select {
    selectProps ={
        className: 'select-doctor-filter'
    }
    render(){
        const el = new Select(this.selectProps)
        const element = el.render()
        const optionDefault = new Option('Select doctor')
        const optionDentist = new Option('Dentist', 'dentist');
        const optionCardiologist = new Option('Cardiologist', 'cardiologist');
        const optionTherapist = new Option('Therapist', 'therapist')
        element.append(optionDefault,optionDentist,optionCardiologist,optionTherapist)
        return element
    }
}