import {Form} from '../form/form.js';
import {DentistVisitForm} from "../form/dentistVisitForm.js";
import {TherapistVisitForm} from "../form/therapistVisitForm.js";
import {CardioVisitForm} from "../form/cardioVisitForm.js";


export class SelectForm extends Form {
    formProps = {
        className: 'form select-doctor-form'
    }

    render() {
        const select = document.createElement('select')
        select.className = 'select-doctor'
        this.select = select.value
        const optionDefault = new Option('Enter your doctor')
        const optionDentist = new Option('Dentist', 'dentist');
        const optionCardiologist = new Option('Cardiologist', 'cardiologist');
        const optionTherapist = new Option('Therapist', 'therapist')
        const formObj = new Form(this.formProps)
        const form = formObj.render()
        select.addEventListener('change', function () {
            const formEl = form.querySelector('form')

            if (this.value === 'dentist') {
                if(formEl){
                    formEl.remove()
                }
                const newVisit = new DentistVisitForm()
                const visit = newVisit.render()
                select.after(visit)
            }
            if (this.value === 'therapist') {
                if(formEl){
                    formEl.remove()
                }
                const newVisit = new TherapistVisitForm()
                const visit = newVisit.render()
                select.after(visit)
            }
            if (this.value === 'cardiologist') {
                if(formEl){
                    formEl.remove()
                }
                const newVisit = new CardioVisitForm()
                const visit = newVisit.render()
                select.after(visit)
            }
        })
        select.append(optionDefault, optionCardiologist, optionDentist, optionTherapist)
        form.append(select)
        return form
    }
}
