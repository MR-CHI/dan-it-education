import {Component} from "../component.js";
import {Button} from "../buttons/button.js";
import {request} from "../axios/axios.js";

export class Visit extends Component {
    cardProps = {
        className: 'card-wrapper',
        draggable: true
    }
    changeButtonProps = {
        className: 'change-card-btn',
        content: 'Change'
    }
    showMoreButtonProps = {
        className: 'show-more-btn',
        content: 'Show more info'
    }

    drag = (e) => {
        e.target.dataset.draggableId = "targetObj";
        e.dataTransfer.setData("draggableId", "targetObj");
    }

    render() {
        const element = this.createElement('div', this.cardProps);
        const changeButtonObj = new Button(this.changeButtonProps)
        const showButtonObj = new Button(this.showMoreButtonProps)
        const showBtn = showButtonObj.render()
        const changeBtn = changeButtonObj.render()
        showBtn.addEventListener('click', function () {
            const elemToShow = element.querySelectorAll('.hidden-info')
            elemToShow.forEach(el => el.style.display = 'block')
            this.remove()
        })
        const close = document.createElement('span')
        close.className = 'close'
        close.textContent = 'X'
        close.addEventListener('click',function () {
                    element.remove()
                    const id = element.getAttribute('data-id')
                    request.delete(`/cards/${id}`).then(({data}) => console.log(data))
                }
            )
        element.append(close, showBtn, changeBtn)
        element.addEventListener('dragstart',this.drag)
        return element
    }
}
