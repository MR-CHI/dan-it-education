import {Visit} from "./visit.js";
import {TherapistVisitForm} from "../form/therapistVisitForm.js";
import {ModalAddVisit} from "../modal/modalAddVisit.js";
import {request} from "../axios/axios.js";

export class VisitTherapist extends Visit{
    visitProps = TherapistVisitForm.body
    render(){
        const empty = document.querySelector('.empty-text')
        if(empty){
            empty.remove()
        }
        const {doctor, selectPriority, person, birthday, purpose, description} = this.visitProps
        const card = super.render()
        card.insertAdjacentHTML('afterbegin', `<h3 class="doctor">Доктор: ${doctor}</h3>
                                                            <p class="patient-name">ФИО пациента: ${person}</p>
                                                            <p class="hidden-info priority">Приоритет ${selectPriority}</p>
                                                            <p class="hidden-info purpose">Цель Визита: ${purpose}</p>
                                                            <p class="hidden-info description">Краткое описание визита: ${description}</p>
                                                            <p class="hidden-info birth-date">Дата рождения: ${birthday}</p>`)
        const changeBtn = card.querySelector('.change-card-btn')
        changeBtn.addEventListener('click', function () {
            const id = card.getAttribute('data-id')
            const modalObj = new ModalAddVisit()
            const modal = modalObj.render()
            modal.innerHTML = `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                 </button>`
            const closeBtn = modal.querySelector('.close')
            closeBtn.addEventListener('click', function(){
                modal.remove()
            })
            request.get(`/cards/${id}`).then(({data}) => {
                const formObj = new TherapistVisitForm()
                formObj.submitProps.value = 'Change'
                console.log(data.selectPriority)

                formObj.person.value = data.person
                formObj.dataBirthday.value = data.birthday
                formObj.purposeVisit.value = data.purpose
                formObj.descriptionProps.value = data.description
                const form = formObj.render()
                const select = form.querySelector('.selectTherapist')
                const option = [...select.querySelectorAll('option')]
                const selected = data.selectPriority
                option.forEach(el =>{
                    if(el.value.includes(selected)){
                        el.setAttribute('selected', 'true')
                    }
                })
                form.addEventListener('submit', function (e) {
                    e.preventDefault()
                    const person = this.querySelector('[name="user-name"]').value
                    const purpose = this.querySelector('[name="visit-purpose"]').value
                    const birthday = this.querySelector('[name="user-age"]').value
                    const description = this.querySelector('[name="user-description"]').value
                    const selectPriority = this.querySelector('.selectTherapist').value
                    const body = {
                        doctor: 'therapist',
                        person, purpose, birthday, description, selectPriority
                    }
                    request.put(`/cards/${id}`, body).then(({data}) => {
                        const {person, selectPriority, purpose, birthday, description} = data
                        const patient = card.querySelector('.patient-name')
                        patient.textContent = `ФИО пациента: ${person}`
                        const priority = card.querySelector('.priority')
                        priority.textContent = `Приоритет ${selectPriority}`
                        const purposeEl = card.querySelector('.purpose')
                        purposeEl.textContent = `Цель визита: ${purpose}`
                        const birthdayEl = card.querySelector('.birth-date')
                        birthdayEl.textContent = `Дата рождения: ${birthday}`
                        const descriptionEl = card.querySelector('.description')
                        descriptionEl.textContent = `Краткое описание: ${description}`
                        form.remove()
                        modal.remove()
                    })
                })
                modal.append(form)
                document.body.append(modal)
            })
        })
        return card
    }
}