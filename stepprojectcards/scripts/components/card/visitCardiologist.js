import {Visit} from "./visit.js";
import {CardioVisitForm} from "../form/cardioVisitForm.js";
import {ModalAddVisit} from "../modal/modalAddVisit.js";
import {request} from "../axios/axios.js";

export class VisitCardiologist extends Visit{
    visitProps = CardioVisitForm.body
    render(){
        const empty = document.querySelector('.empty-text')
        if(empty){
            empty.remove()
        }
        const {doctor, person, pressure, birthday, bodyMassIndex, pastIllnesses, purpose, description, selectPriority} = this.visitProps
        const card = super.render()
        card.insertAdjacentHTML('afterbegin', `<h3 class="doctor">Доктор: ${doctor}</h3>
                                                            <p class="patient-name">ФИО Пациента ${person}</p>
                                                            <p class="hidden-info priority">Приоритет ${selectPriority}</p>
                                                            <p class="hidden-info birth-date">Дата рождения пациента ${birthday}</p>
                                                            <p class="hidden-info pressure">Давление ${pressure}</p>
                                                            <p class="hidden-info body-index">Индекс массы тела ${bodyMassIndex}</p>
                                                            <p class="hidden-info illness">Перенесенные заболевания сердечно-сосудистой системы ${pastIllnesses}</p>
                                                            <p class="hidden-info purpose">Цель визита ${purpose}</p>
                                                            <p class="hidden-info description">Краткое описание визита ${description}</p>`)
        const changeBtn = card.querySelector('.change-card-btn')
        changeBtn.addEventListener('click', function(){
            const id = card.getAttribute('data-id')
            const modalObj = new ModalAddVisit()
            const modal = modalObj.render()
            modal.innerHTML = `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                 </button>`
            const closeBtn = modal.querySelector('.close')
            closeBtn.addEventListener('click', function(){
                modal.remove()
            })
            request.get(`/cards/${id}`).then(({data}) => {
                const formObj = new CardioVisitForm()
                formObj.submitProps.value = 'Change'
                formObj.person.value = data.person
                formObj.agePerson.value = data.birthday
                formObj.pressure.value = data.pressure
                formObj.bodyMassIndex.value = data.bodyMassIndex
                formObj.pastIllnesses.value = data.pastIllnesses
                formObj.purposeVisit.value = data.purpose
                formObj.descriptionProps.value = data.description
                const form = formObj.render()
                const select = form.querySelector('.selectCardio')
                const option = [...select.querySelectorAll('option')]
                const selected = data.selectPriority
                option.forEach(el =>{
                    if(el.value.includes(selected)){
                        el.setAttribute('selected', 'true')
                    }
                })
                form.addEventListener('submit', function (e) {
                    e.preventDefault()
                    const person = this.querySelector('[name="user-name"]').value
                    const purpose = this.querySelector('[name="visit-purpose"]').value
                    const birthday = this.querySelector('[name="user-age"]').value
                    const bodyMassIndex = this.querySelector('[name="user-body"]').value
                    const pastIllnesses = this.querySelector('[name="user-illness"]').value
                    const description = this.querySelector('[name="user-description"]').value
                    const selectPriority = this.querySelector('.selectCardio').value
                    const pressure = this.querySelector('[name="user-pressure"]').value
                    const body = {
                        doctor: 'cardiologist',
                        person, purpose, birthday, description, selectPriority,bodyMassIndex,pastIllnesses,pressure
                    }
                    request.put(`/cards/${id}`, body).then(({data}) => {
                        const {person, purpose, birthday, description, selectPriority,bodyMassIndex,pastIllnesses,pressure} = data
                        const patient = card.querySelector('.patient-name')
                        patient.textContent = `ФИО пациента: ${person}`
                        const priority = card.querySelector('.priority')
                        priority.textContent = `Приоритет ${selectPriority}`
                        const purposeEl = card.querySelector('.purpose')
                        purposeEl.textContent = `Цель визита: ${purpose}`
                        const descriptionEl = card.querySelector('.description')
                        descriptionEl.textContent = `Краткое описание: ${description}`
                        const birthdayEl = card.querySelector('.birth-date')
                        birthdayEl.textContent = `Дата рождения пациента ${birthday}`
                        const pressureEl = card.querySelector('.pressure')
                        pressureEl.textContent = `Давление ${pressure}`
                        const bodyInd = card.querySelector('.body-index')
                        bodyInd.textContent = `Индекс массы тела ${bodyMassIndex}`
                        const illness = card.querySelector('.illness')
                        illness.textContent = `Перенесенные заболевания сердечно-сосудистой системы ${pastIllnesses}`
                        form.remove()
                        modal.remove()
                    })
                })
                modal.append(form)
                document.body.append(modal)
            })
        })
        return card
    }
}