import {Form} from "./form.js";
import {Input} from "../input/input.js";
import {Textarea} from "../textarea/textarea.js";
import {request} from "../axios/axios.js";
import {VisitTherapist} from "../card/visitTherapist.js";

export class TherapistVisitForm extends Form{
    submitProps = {
        type: 'submit',
        value: 'Create visit'
    }
    person = {
        type: 'text',
        name: "user-name",
        placeholder: 'Фамилия Имя Отчество'
    }
    descriptionProps = {
        name: 'user-description',
        placeholder: 'Краткое описание визита'
    }
    purposeVisit = {
        type: 'text',
        name: 'visit-purpose',
        placeholder: 'Цель визита'
    }
    formProps ={
        className: 'form therapist-form'
    }
    dataBirthday = {
        name: 'user-age',
        type: 'text',
        placeholder: 'Ваш возраст'
    }
    render(){
        const {person, descriptionProps ,purposeVisit,formProps, dataBirthday} = this;
        const inputBirthday = new Input(dataBirthday).render()
        const inputName = new Input(person).render()
        const inputPurpose = new Input(purposeVisit).render()
        const textarea = new Textarea(descriptionProps).render()
        const formObj = new Form(formProps)
        const form = formObj.render()
        const submit = new Input(this.submitProps)
        const select = document.createElement('select')
        select.className = 'selectTherapist'
        const optionDefaults = new Option('Введите срочность')
        const optionCommon = new Option('Обычная')
        const optionPriority = new Option('Приоритетная')
        const optionUrgent = new Option('Неотложная')
        select.append(optionDefaults, optionCommon, optionPriority,optionUrgent)
        form.append(select,inputName,inputPurpose, textarea, inputBirthday, submit.render());
        form.addEventListener('submit', function(e){
            e.preventDefault()
            if (!inputName.value || !inputPurpose.value || !inputBirthday.value || !textarea.value || select.value === 'Введите срочность') {
                const err = document.createElement("p")
                err.className = 'form-error'
                err.textContent = 'all fields should be filled in'
                form.append(err)
            } else {
                const selectDoc = document.querySelector('.select-doctor')
                const body = {
                    doctor: selectDoc.value,
                    selectPriority: select.value,
                    person: inputName.value,
                    birthday: inputBirthday.value,
                    purpose: inputPurpose.value,
                    description: textarea.value,
                }
                TherapistVisitForm.body = body
                request.post('/cards', body).then(({data}) => {
                    const cardObj = new VisitTherapist()
                    const card = cardObj.render()
                    card.setAttribute('data-id', data.id)
                    const container = document.querySelector('.cards')
                    container.append(card)
                    form.remove()
                    const modal = document.querySelector('.add-visit-modal')
                    modal.remove()
                })
            }
        })
        return form
    }
}