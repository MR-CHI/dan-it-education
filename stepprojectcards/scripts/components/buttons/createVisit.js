import {Button} from "./button.js";

export class CreateVisitBtn extends Button{
    render(){
        const btn = super.render(this.props)
        this.elem = btn
        return btn
    }
}