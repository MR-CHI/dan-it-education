const token = localStorage.getItem('token')

export const request = axios.create({
    baseURL: "http://cards.danit.com.ua",
    headers: {
        Authorization: `Bearer ${token}`,
    }
});


