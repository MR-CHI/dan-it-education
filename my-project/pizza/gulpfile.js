const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const rigger = require('gulp-rigger');
const cleanCss = require('gulp-clean-css');
const del = require('del');
const concat = require('gulp-concat')
const terser = require('gulp-terser');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');



const scssBuild = () => {
    return gulp.src('src/css/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css/'));
};
const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
};

const imageminBuild = ()=>{
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img/'))
        // .pipe(cleanCss({compatibility: 'ie8'}));
};

const watch =() =>{
    gulp.watch('src/css/**/*.*', scssBuild).on('change', browserSync.reload);
    gulp.watch('*.html', browserSync.reload);
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
};

gulp.task('dev', watch);
gulp.task('build', gulp.series(scssBuild, imageminBuild, jsBuild));




